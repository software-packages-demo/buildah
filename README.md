# [containers/buildah](https://github.com/containers/buildah)

OCI images CLI building

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=docker+podman+buildah+skopeo&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

# Official documentation
* Man pages
* [*Troubleshooting: A list of common issues and solutions for Buildah*
  ](https://github.com/containers/buildah/blob/main/troubleshooting.md)
* [Buildah Tutorials](https://github.com/containers/buildah/tree/master/docs/tutorials)
  * Include Buildah in your build tool (= using as a Golang library)
* [*Chapter 6. Building container images with Buildah*
  ](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/building_running_and_managing_containers/building-container-images-with-buildah_building-running-and-managing-containers)
* [*Chapter 1. Finding, Running, and Building Containers with podman, skopeo, and buildah*
  ](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux_atomic_host/7/html/managing_containers/finding_running_and_building_containers_with_podman_skopeo_and_buildah)

# Semi-official documentation
* [*Speeding up container image builds with Buildah*
  ](https://www.redhat.com/sysadmin/speeding-container-buildah)
  2020-03 Dan Walsh
* [*Say “Hello” to Buildah, Podman, and Skopeo*
  ](https://servicesblog.redhat.com/2019/10/09/say-hello-to-buildah-podman-and-skopeo/)
  2019-10 Saharsh
* [*Best practices for running Buildah in a container*
  ](https://developers.redhat.com/blog/2019/08/14/best-practices-for-running-buildah-in-a-container/)
  2019-08 Daniel Walsh

# Unofficial documentation
* [*Building Debian Packages with buildah*
  ](https://tauware.blogspot.com/2020/04/building-packages-with-buildah-in-debian.html)
  2020-04 Reinhard Tartler
* [*How to run podman from inside a container*
  ](https://stackoverflow.com/questions/56032747/how-to-run-podman-from-inside-a-container)
  (2019) Stack Overflow

# Running inside a container
* [*How to run podman from inside a container*
  ](https://stackoverflow.com/questions/56032747/how-to-run-podman-from-inside-a-container)
  (2019) Stack Overflow
* [*Best practices for running Buildah in a container*
  ](https://developers.redhat.com/blog/2019/08/14/best-practices-for-running-buildah-in-a-container/)
  2019-08 Daniel Walsh

## Install iptables ?? with package manager (apt, dnf, or similar)

## Storage driver
### `storage.conf`
    [storage]
    driver = ...
If there is no `storage.conf`, `vfs` seems often to be choosen. `driver = ""` may also result in `vfs` been choosen.

### cli: --storage-driver=...
### info about configuration
    "store":
    ...
        "GraphDriverName":"..."
### Drivers
#### vfs
* Maybe slow
* The only one I am sure I've been able to make work.

#### overlay
* I am not sure if I've been able to make it work.
